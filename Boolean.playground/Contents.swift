import Cocoa
import Darwin

// Create with Type Annotation
var truth: Bool

// Flip Boolean with ! or toggle
var isAuthenticated = false
isAuthenticated = !isAuthenticated
isAuthenticated.toggle()

